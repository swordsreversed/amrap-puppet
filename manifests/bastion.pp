# == Class: amrappages
class bastion {

    $systemHostName = 'rws-bastion'

    #Ensure following packages are installed
    package { ['git', 'vim', 'wget', 'curl', 'software-properties-common']:
        ensure => installed,
    }

    #Debian doesn't have the mysql client tools for 5.6 yet
    #This is a precompiled binary to ensure we can run our nighly backups
    exec{ 'get_mysqldump':
        path    => '/usr/bin',
        command => 'aws s3 cp s3://rws-releases/binaries/mysqldump /tmp/',
        creates => '/tmp/mysqldump',
    }

    # Set file permissions for mysqldump
    file { '/usr/local/bin/mysqldump':
        ensure  => file,
        source  => '/tmp/mysqldump',
        mode    => '0700',
        require => Exec['get_mysqldump']
    }

    #Install backup script and enable cronjob
    file { '/root/backupDatabase.sh':
        ensure => file,
        mode   => '0600',
        source => 'puppet:///modules/amrappages/backupDatabase.sh',
    }

    # cron { 'nightly-database-backup':
    #     command => '/root/backupDatabase.sh',
    #     user    => 'root',
    #     hour    => 0,
    #     minute  => 30,
    #     require => File['/root/backupDatabase.sh']
    # }

    exec { 'set system hostname':
        command => "/bin/hostname ${systemHostName}"
    }

    host { $systemHostName:
        ip => '127.0.0.1',
    }

    class { 'amrappages::timezone':
        timezone => 'Australia/Sydney'
    }

    class { 'amrappages::smtp': }

    class { 'amrappages::awslogs':
        awsLogsConfigFilePath => "puppet:///modules/amrappages/awslogs-${systemHostName}.conf"
    }
}
