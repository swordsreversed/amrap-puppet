class audioserver::db {

    class { '::mysql::server':

        # Set the root password
        root_password => $audioserver::conf::root_password,

        # Create the database
        databases => {
            "${audioserver::conf::db_name}" => {
                ensure => 'present',
                charset => 'utf8'
            }
        },

        # Create the user
        users => {
            "${audioserver::conf::db_user_host}" => {
                ensure => present,
                password_hash => mysql_password("${audioserver::conf::db_user_password}")
            }
        },

        # Grant privileges to the user
        grants => {
            "${audioserver::conf::db_user_host_db}" => {
                ensure     => 'present',
                options    => ['GRANT'],
                privileges => ['ALL'],
                table      => "${audioserver::conf::db_name}.*",
                user       => "${audioserver::conf::db_user_host}",
            }
        },
    }

    # Install MySQL client and all bindings
    class { '::mysql::client':
        require => Class['::mysql::server'],
        bindings_enable => true
    }
}
