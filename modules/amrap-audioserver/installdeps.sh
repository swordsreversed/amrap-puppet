#!/usr/bin/env bash

sudo puppet module install puppetlabs-stdlib
sudo puppet module install puppetlabs-apt
sudo puppet module install puppetlabs-apache
sudo puppet module install puppetlabs-mysql
sudo puppet module install willdurand-nodejs
sudo puppet module install wilrnh-ffmpeg --version 0.1.2
